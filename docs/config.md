# Configuration

## Path

Config saved to user's config dir, for Linux it is `~/.config/mitufe/`

## Structure

```json
{
  "server": "https://miniflux-server.site",
  "login": "user_login",
  "password": "user_password",
  "token": "API token",
  "sort": {
    "order": "field_name",
    "direction": "desc"
  },
  "colors": {
    "sidebar_background": "black",
    "sidebar_text": "white",
    "main_background": "black",
    "main_text": "white",
    "article_background": "black",
    "article_text": "white",
    "statusbar_background": "black",
    "statusbar_text": "white",
    "hotkeybar_background": "black",
    "hotkeybar_text": "white"
  },
  "hotkeys": {
    "up": "k",
    "down": "j",
    "top": "g",
    "bottom": "G",
    "toggle": "T",
    "close": "q",
    "next_article": "l",
    "open_in_browser": "o"
  },
  "elements": {
    "sidebar": true,
    "statusbar": true,
    "hotkeybar": true
  },
  "date_format": "golang date format"
}
```

## Fields

### server

URL of miniflux server, just host with http/https scheme

### token

Miniflux API token, **preferred** auth way.

> **NOTE**: if you set token, you don't need to provide login and password

### login

Miniflux user login

> **NOTE**: if you set token, you don't need to provide login and password

### password

Miniflux user password

> **NOTE**: if you set token, you don't need to provide login and password

### sort

* `order` - Miniflux entry's field name to use for sort, allowed values: `id`, `status`, `published_at`, `category_title`, `category_id`
* `direction` - Sorting direction, allowed values: `asc`, `desc`

### colors

Color configuration for each one of the TUI elements, allowed values: `black`, `maroon`, `green`, `olive`, `navy`, `purple`, `teal`,
`silver`, `gray`, `red`, `lime`, `yellow`, `blue`, `fuchsia`, `aqua`, `white`, `aliceblue`, `antiquewhite`, `aquamarine`, `azure`,
`beige`, `bisque`, `blanchedalmond`, `blueviolet`, `brown`, `burlywood`, `cadetblue`, `chartreuse`, `chocolate`, `coral`,
`cornflowerblue`, `cornsilk`, `crimson`, `darkblue`, `darkcyan`, `darkgoldenrod`, `darkgray`, `darkgreen`, `darkkhaki`,
`darkmagenta`, `darkolivegreen`, `darkorange`, `darkorchid`, `darkred`, `darksalmon`, `darkseagreen`, `darkslateblue`,
`darkslategray`, `darkturquoise`, `darkviolet`, `deeppink`, `deepskyblue`, `dimgray`, `dodgerblue`, `firebrick`,
`floralwhite`, `forestgreen`, `gainsboro`, `ghostwhite`, `gold`, `goldenrod`, `greenyellow`, `honeydew`, `hotpink`,
`indianred`, `indigo`, `ivory`, `khaki`, `lavender`, `lavenderblush`, `lawngreen`, `lemonchiffon`, `lightblue`,
`lightcoral`, `lightcyan`, `lightgoldenrodyellow`, `lightgray`, `lightgreen`, `lightpink`, `lightsalmon`, `lightseagreen`,
`lightskyblue`, `lightslategray`, `lightsteelblue`, `lightyellow`, `limegreen`, `linen`, `mediumaquamarine`, `mediumblue`,
`mediumorchid`, `mediumpurple`, `mediumseagreen`, `mediumslateblue`, `mediumspringgreen`, `mediumturquoise`, `mediumvioletred`,
`midnightblue`, `mintcream`, `mistyrose`, `moccasin`, `navajowhite`, `oldlace`, `olivedrab`, `orange`, `orangered`, `orchid`,
`palegoldenrod`, `palegreen`, `paleturquoise`, `palevioletred`, `papayawhip`, `peachpuff`, `peru`, `pink`, `plum`, `powderblue`,
`rebeccapurple`, `rosybrown`, `royalblue`, `saddlebrown`, `salmon`, `sandybrown`, `seagreen`, `seashell`, `sienna`, `skyblue`,
`slateblue`, `slategray`, `snow`, `springgreen`, `steelblue`, `tan`, `thistle`, `tomato`, `turquoise`, `violet`, `wheat`,
`whitesmoke`, `yellowgreen`, `grey`, `dimgrey`, `darkgrey`, `darkslategrey`, `lightgrey`, `lightslategrey`, `slategrey`.

default values: `black` for background, `white` for text

### hotkeys

Hotkeys/shortcuts configuration:

* `up` - move up
* `down` - move down
* `top` - move to the top
* `bottom` - move to the bottom
* `toggle` - toggle articles (read/unread)
* `close` - close current view or app
* `next_article` - switch to next article in article view
* `open_in_browser` - open current article in browser

Check default values in the config structure above

### elements

Enable/disable following TUI elements:

* Sidebar, default: `true` (enabled)
* Statusbar, default: `true` (enabled)
* Hotkeybar, default: `true` (enabled)

### date_format

Go's date format, full list available [on time package documentation](https://golang.org/pkg/time/#pkg-constants),
example list of allowed values:

* `Mon Jan _2 15:04:05 2006`
* `Mon Jan _2 15:04:05 MST 2006`
* `Mon Jan 02 15:04:05 -0700 2006`
* `02 Jan 06 15:04 MST`
* `02 Jan 06 15:04 -0700`
* `Monday, 02-Jan-06 15:04:05 MST`
* `Mon, 02 Jan 2006 15:04:05 MST`
* `Mon, 02 Jan 2006 15:04:05 -0700`
* `2006-01-02T15:04:05Z07:00`
* `2006-01-02T15:04:05.999999999Z07:00`
* `3:04PM`
* `Jan _2 15:04:05`
* `Jan _2 15:04:05.000`
* `Jan _2 15:04:05.000000`
* `Jan _2 15:04:05.000000000`

> **NOTE**: you should copy exact value string (yes, with "predefined" date) to make it work as expected

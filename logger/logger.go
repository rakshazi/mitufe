package logger

import (
	"fmt"
	"os"
	"time"
)

// Logger definition
type Logger struct {
	Path string
	file *os.File
}

// New logger
func New(app string) (*Logger, error) {
	path, err := GetPath(app)
	if err != nil {
		return nil, err
	}
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.Mkdir(path, 0700)
		if err != nil {
			return nil, err
		}
	}
	path += "/" + time.Now().Format("2006-01-02.log")
	file, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}

	logger := &Logger{
		Path: path,
		file: file,
	}

	return logger, nil
}

// Writeln - write line in log file
func (l *Logger) Writeln(message string) error {
	line := time.Now().Format("2006-01-02 15:04:05.000 ") + message + "\n"
	_, err := l.file.WriteString(line)
	return err
}

// Println - prints line to STDOUT and writes it to logfile
func (l *Logger) Println(message string) error {
	line := time.Now().Format("2006-01-02 15:04:05 ") + message
	fmt.Println(line)
	return l.Writeln(message)
}

// Close log file
func (l *Logger) Close() error {
	return l.file.Close()
}

// GetPath to logs dir
func GetPath(app string) (string, error) {
	path, err := os.UserCacheDir()
	if err != nil {
		return path, err
	}
	path += "/" + app
	return path, nil
}

package config

// default hotkeys
var defaultHotkeys Hotkeys = Hotkeys{
	Up:            "k",
	Down:          "j",
	Top:           "g",
	Bottom:        "G",
	Toggle:        "T",
	Close:         "q",
	NextArticle:   "l",
	OpenInBrowser: "o",
}

// default elements config
var defaultElements Elements = Elements{
	Sidebar:   true,
	Statusbar: true,
	Hotkeybar: true,
}

// default sort options
var defaultSort Sort = Sort{
	Order:     "published_at",
	Direction: "asc",
}

// default colors
var defaultColors Colors = Colors{
	SidebarBackground:   "black",
	SidebarText:         "white",
	MainBackground:      "black",
	MainText:            "white",
	ArticleBackground:   "black",
	ArticleText:         "white",
	StatusbarBackground: "black",
	StatusbarText:       "white",
	HotkeybarBackground: "darkgray",
	HotkeybarText:       "black",
}

package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

// ConfigFile - config file name
const ConfigFile string = "/config.json"

// New Config instance
func New(app, server, login, password, token, sortOrder, sortDirection, dateFormat string) (*Config, error) {
	path, err := GetPath(app)
	if err != nil {
		return nil, err
	}
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.Mkdir(path, 0700)
		if err != nil {
			return nil, err
		}
	}
	path += ConfigFile

	// If new config vars - save them
	if server != "" {
		config := &Config{
			Server:   server,
			Login:    login,
			Password: password,
			Token:    token,
			Sort: Sort{
				Order:     sortOrder,
				Direction: sortDirection,
			},
			Colors:     defaultColors,
			Hotkeys:    defaultHotkeys,
			Elements:   defaultElements,
			DateFormat: dateFormat,
		}
		err := Write(path, config)
		return config, err
	}
	// If no new vars - read existing config
	return Read(path)
}

// Read config file
func Read(path string) (*Config, error) {
	var config *Config = &Config{Sort: defaultSort, Colors: defaultColors, Hotkeys: defaultHotkeys, Elements: defaultElements}
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return config, nil
	}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return config, err
	}
	json.Unmarshal(data, &config)

	return config, nil
}

// Write config to file
func Write(path string, config *Config) error {
	jsonString, err := json.Marshal(config)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(path, jsonString, 0660)
}

// GetPath to config dir
func GetPath(app string) (string, error) {
	path, err := os.UserConfigDir()
	if err != nil {
		return path, err
	}
	path += "/" + app
	return path, nil
}

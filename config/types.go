package config

// Config
type Config struct {
	Server     string   `json:"server,omitempty"`
	Login      string   `json:"login,omitempty"`
	Password   string   `json:"password,omitempty"`
	Token      string   `json:"token,omitempty"`
	Sort       Sort     `json:"sort,omitempty"`
	Hotkeys    Hotkeys  `json:"hotkeys,omitempty"`
	Colors     Colors   `json:"colors,omitempty"`
	Elements   Elements `json:"elements,omitempty"`
	DateFormat string   `json:"date_format,omitempty"`
}

// Hotkeys config
type Hotkeys struct {
	Up            string `json:'up,omitempty"`
	Down          string `json:"down,omitempty"`
	Top           string `json:"top,omitempty"`
	Bottom        string `json:"bottom,omitempty"`
	Toggle        string `json:"toggle,omitempty"`
	Close         string `json:"close,omitempty"`
	NextArticle   string `json:"next_article,omitempty"`
	OpenInBrowser string `json:"open_in_browser,omitempty"`
}

// Elements - list of enabled/disabled TUI elements
type Elements struct {
	Sidebar   bool `json:"sidebar,omitempty"`
	Statusbar bool `json:"statusbar,omitempty"`
	Hotkeybar bool `json:"hotkeybar,omitempty"`
}

// Sort config
type Sort struct {
	Order     string `json:"order,omitempty"`
	Direction string `json:"direction,omitempty"`
}

// Colors config
type Colors struct {
	SidebarBackground   string `json:"sidebar_background,omitempty"`
	SidebarText         string `json:"sidebar_text,omitempty"`
	MainBackground      string `json:"main_background,omitempty"`
	MainText            string `json:"main_text,omitempty"`
	ArticleBackground   string `json:"article_background,omitempty"`
	ArticleText         string `json:"article_text,omitempty"`
	StatusbarBackground string `json:"statusbar_background,omitempty"`
	StatusbarText       string `json:"statusbar_text,omitempty"`
	HotkeybarBackground string `json:"hotkeybar_background,omitempty"`
	HotkeybarText       string `json:"hotkeybar_text,omitempty"`
}

package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"runtime"
	"testing"
	"time"

	"gitlab.com/rakshazi/mitufe/logger"
)

func TestMain(m *testing.M) {
	cfgPath, _ := GetPath("test")
	logPath, _ := logger.GetPath("test")
	for _, path := range []string{cfgPath, logPath} {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			os.Mkdir(path, 0700)
		}
	}
	defer os.Remove(cfgPath + "/local.db")
	defer os.Remove(logPath + time.Now().Format("/2006-01-02.log"))
	os.Exit(m.Run())
}

func TestNew(t *testing.T) {
	os.Setenv("XDG_CONFIG_HOME", "/tmp")
	defer os.RemoveAll("/tmp/mitufe")
	defer os.Setenv("XDG_CONFIG_HOME", os.Getenv("HOME")+"/.config")
	config, err := New("test", "http://not.exist", "login", "password", "", "published_at", "asc", "02 Jan 06 15:04")
	if err != nil {
		t.Error(err)
	}
	if config.Server != "http://not.exist" {
		t.Fail()
	}
	config2, err := New("test", "", "", "", "", "published_at", "asc", "02 Jan 06 15:04")
	if err != nil {
		t.Error(err)
	}
	if config2.Server != "http://not.exist" {
		t.Fail()
	}
}

func TestGetPath(t *testing.T) {
	// Partial copy of os.UserConfigDir() implementation
	var dir string
	switch runtime.GOOS {
	case "windows":
		dir = os.Getenv("AppData")
	case "darwin", "ios":
		dir = os.Getenv("HOME")
		dir += "/Library/Application Support"
	case "plan9":
		dir = os.Getenv("home")
		dir += "/lib"
	default: // Unix
		dir = os.Getenv("XDG_CONFIG_HOME")
		if dir == "" {
			dir = os.Getenv("HOME")
			dir += "/.config"
		}
	}

	// Actual test
	expected := dir + "/test"
	path, err := GetPath("test")
	if err != nil {
		t.Error(err)
	}
	if path != expected {
		t.Errorf("Expected: %s, but got: %s", expected, path)
	}
}

func TestWrite(t *testing.T) {
	file, err := ioutil.TempFile("", "config.json")
	if err != nil {
		t.Error(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	config := &Config{
		Server: "http://not.exist",
		Token:  "test",
	}
	configFromFile := &Config{}
	err = Write(file.Name(), config)
	if err != nil {
		t.Error(err)
	}
	data, err := ioutil.ReadAll(file)
	if err != nil {
		t.Error(err)
	}
	if err = json.Unmarshal(data, &configFromFile); err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(config, configFromFile) {
		t.Fail()
	}
}

func TestRead(t *testing.T) {
	file, err := ioutil.TempFile("", "config.json")
	if err != nil {
		t.Error(err)
	}
	defer os.Remove(file.Name())
	defer file.Close()
	config := &Config{
		Server:   "http://not.exist",
		Token:    "test",
		Colors:   defaultColors,
		Elements: defaultElements,
		Hotkeys:  defaultHotkeys,
		Sort:     defaultSort,
	}
	jsonString, err := json.Marshal(config)
	if err != nil {
		t.Error(err)
	}
	if err := ioutil.WriteFile(file.Name(), jsonString, 0660); err != nil {
		t.Error(err)
	}
	configFromFile, err := Read(file.Name())
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(config, configFromFile) {
		t.Fail()
	}
}

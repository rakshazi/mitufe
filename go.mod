module gitlab.com/rakshazi/mitufe

go 1.16

require (
	github.com/elliotchance/orderedmap v1.4.0
	github.com/gdamore/tcell/v2 v2.2.0
	github.com/jaytaylor/html2text v0.0.0-20200412013138-3577fbdbcff7
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pkg/browser v0.0.0-20210115035449-ce105d075bb4
	github.com/rivo/tview v0.0.0-20210312174852-ae9464cc3598
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	go.etcd.io/bbolt v1.3.6-0.20210424004142-d043936a71e7
	golang.org/x/net v0.0.0-20210326220855-61e056675ecf // indirect
	golang.org/x/sys v0.0.0-20210423185535-09eb48e85fd7 // indirect
	golang.org/x/term v0.0.0-20210317153231-de623e64d2a6 // indirect
	miniflux.app v0.0.0-20210323043448-6e2e2d166574
)

package main

import (
	"flag"
	"fmt"
	"os"
	"sync"

	"gitlab.com/rakshazi/mitufe/api"
	"gitlab.com/rakshazi/mitufe/config"
	"gitlab.com/rakshazi/mitufe/logger"
	"gitlab.com/rakshazi/mitufe/storage"
	"gitlab.com/rakshazi/mitufe/tui"
)

const app string = "mitufe"

var log *logger.Logger

// Miniflux-related args
var server, login, password, token, sortOrder, sortDirection, dateFormat string
var syncOnly bool

func init() {
	flag.StringVar(&server, "s", "", "Miniflux server URL, example: https://miniflux.app, used only once to generate config")
	flag.StringVar(&login, "u", "", "Miniflux user login, only if you want to use login/password auth, used only once to generate config")
	flag.StringVar(&password, "p", "", "Miniflux user password, only if you want to use login/password auth, used only once to generate config")
	flag.StringVar(&token, "t", "", "Miniflux user API token, only if you want to use token auth, used only once to generate config")
	flag.StringVar(&sortOrder, "sort-order", "published_at", "Field name used for sorting, check miniflux docs for list of available fields")
	flag.StringVar(&sortDirection, "sort-direction", "asc", "Sort direction, allowed values: asc (newest first), desc (oldest first)")
	flag.StringVar(&dateFormat, "date-format", "02 Jan 06 15:04", "Date format, allowed values can be found here: https://golang.org/pkg/time/#pkg-constants")
	flag.BoolVar(&syncOnly, "sync", false, "Only run sync and exit, do not show TUI")
}

func main() {
	var err error
	flag.Parse()
	log, err = logger.New(app)
	if err != nil {
		fmt.Println("[logger]", err)
		os.Exit(1)
	}
	cfg, err := config.New(app, server, login, password, token, sortOrder, sortDirection, dateFormat)
	if err != nil {
		log.Println("[config] " + err.Error())
		os.Exit(1)
	}
	if cfg == nil || cfg.Server == "" {
		log.Println("[config] file not found or cannot be read")
		os.Exit(1)
	}
	var wg sync.WaitGroup
	status := make(chan string)
	client := api.New(cfg)
	store, err := storage.New(app, client, status)
	if err != nil {
		log.Println("[storage] " + err.Error())
		os.Exit(1)
	}
	defer store.Close()
	wg.Add(1)
	go runSync(cfg, client, store, &wg)
	go updateStatus(status)
	if syncOnly {
		wg.Wait()
		os.Exit(0)
	}
	tui.SetColors(cfg.Colors)
	tui.SetElements(cfg.Elements)
	tui.SetHotkeys(cfg.Hotkeys)
	if err := tui.Run(store); err != nil {
		log.Println("[tui] " + err.Error())
		os.Exit(1)
	}
}

func runSync(cfg *config.Config, client api.Client, store storage.Store, wg *sync.WaitGroup) {
	// Run sync only if have internet access
	if _, err := client.Me(); err == nil {
		if err := store.Sync(cfg.DateFormat, &cfg.Sort); err != nil {
			log.Println("[storage] sync " + err.Error())
		}
	}
	wg.Done()
}

func updateStatus(status chan string) {
	for message := range status {
		if syncOnly {
			log.Println(message)
		} else {
			log.Writeln(message)
			tui.SetStatus(message)
		}
	}
}

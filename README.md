# mitufe

[![GoDoc](https://img.shields.io/badge/godoc-reference-informational.svg?style=for-the-badge)](https://pkg.go.dev/gitlab.com/rakshazi/mitufe)
[![Download binary](https://img.shields.io/badge/download-binary-success.svg?style=for-the-badge)](https://gitlab.com/rakshazi/mitufe/-/releases)
[![Download docker](https://img.shields.io/badge/download-docker-blue.svg?style=for-the-badge)](https://gitlab.com/rakshazi/mitufe/container_registry)
[![Liberapay](https://img.shields.io/badge/donate-liberapay-yellow.svg?style=for-the-badge)](https://liberapay.com/rakshazi)
[![coverage report](https://gitlab.com/rakshazi/mitufe/badges/master/coverage.svg)](https://gitlab.com/rakshazi/mitufe/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/rakshazi/mitufe)](https://goreportcard.com/report/gitlab.com/rakshazi/mitufe)


CLI/TUI rss reader, offline-first, highly customizable

## Documentation

### Supported backends

* **Miniflux** ([miniflux.app](https://miniflux.app)) - a minimalist and opionated feed reader.

#### Planned

following backends/providers/APIs are not implemented yet, but I have plans to implement them:

* **Fever API** - supported by multiple backends

### Install

#### GNU/Linux distributives' packages

Available on [releases](https://gitlab.com/rakshazi/mitufe/-/releases) page

#### Docker

```bash
docker run -it --rm registry.gitlab.com/rakshazi/mitufe -h
```

#### Go

```bash
go install gitlab.com/rakshazi/mitufe@latest
mitufe -h
```

> **NOTE**: configuration and local db saved into `/home/mitufe/.config/mitufe` directory,
> don't forget to use it as volume

### Usage

```bash
Usage of mitufe:
  --sync
        Only sync, do not show TUI, just update db and exit
  --date-format string
        Date format, allowed values can be found here: https://golang.org/pkg/time/#pkg-constants (default "02 Jan 06 15:04")
  --sort-direction string
        Sort direction, allowed values: asc (newest first), desc (oldest first) (default "asc")
  --sort-order string
        Field name used for sorting, check miniflux docs for list of available fields (default "published_at")
  -p string
        Miniflux user password, only if you want to use login/password auth, used only once to generate config
  -s string
        Miniflux server URL, example: https://miniflux.app, used only once to generate config
  -t string
        Miniflux user API token, only if you want to use token auth, used only once to generate config
  -u string
        Miniflux user login, only if you want to use login/password auth, used only once to generate config
```

#### keyboard hotkeys

this app uses vim-like controls

**main view**:

* `j`, `down arrow` - Move down.
* `k`, `up arrow` - Move up.
* `g`, `home` - Move to the top.
* `G`, `end` - Move to the bottom.
* `T` - Toggle entries, show unread or read entries
* `Ctrl+j` - Next feed
* `Ctrl+k` - Previous feed
* `Ctrl+g` - First feed
* `q`, `ESC`, `Ctrl+C` - Close app

**article view**:

* `l` - Next article.
* `j`, `down arrow` - Move down.
* `k`, `up arrow` - Move up.
* `g`, `home` - Move to the top.
* `G`, `end` - Move to the bottom.
* `Ctrl-F`, `page down` - Move down by one page.
* `Ctrl-B`, `page up` - Move up by one page.
* `q`, `ESC` - Close article view.

Check the documentation - [Configuration](./docs/config.md)

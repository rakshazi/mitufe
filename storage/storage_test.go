package storage

import (
	"os"
	"testing"
	"time"

	"gitlab.com/rakshazi/mitufe/api"
	"gitlab.com/rakshazi/mitufe/config"
	"gitlab.com/rakshazi/mitufe/logger"
)

func TestMain(m *testing.M) {
	cfgPath, _ := config.GetPath("test")
	logPath, _ := logger.GetPath("test")
	for _, path := range []string{cfgPath, logPath} {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			os.Mkdir(path, 0700)
		}
	}
	defer os.Remove(cfgPath + "/local.db")
	defer os.Remove(logPath + time.Now().Format("/2006-01-02.log"))
	os.Exit(m.Run())
}

func TestNew(t *testing.T) {
	config := &config.Config{Server: "http://not.exist", Token: "test"}
	client := api.New(config)
	store, err := New("test", client, make(chan string))
	if err != nil {
		t.Error(err)
	}
	defer store.Close()
	if _, ok := store.(Store); !ok {
		t.Fail()
	}
}

func TestClose(t *testing.T) {
	config := &config.Config{Server: "http://not.exist", Token: "test"}
	client := api.New(config)
	store, err := New("test", client, make(chan string))
	if err != nil {
		t.Error(err)
	}
	if err := store.Close(); err != nil {
		t.Error(err)
	}
}

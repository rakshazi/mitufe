package storage

import (
	"github.com/elliotchance/orderedmap"
	"gitlab.com/rakshazi/mitufe/api"
	"gitlab.com/rakshazi/mitufe/config"
	"gitlab.com/rakshazi/mitufe/storage/bolt"
)

// Store interface
type Store interface {
	// database-related stuff
	Init() error
	Sync(string, *config.Sort) error
	Close() error
	// feeds
	GetFeeds() (*orderedmap.OrderedMap, error)
	// entries
	GetEntries(string) (*orderedmap.OrderedMap, error)
	MarkEntry(string, string) error
}

// New store
func New(app string, client api.Client, status chan string) (Store, error) {
	store := &bolt.Bolt{App: app, Client: client, Status: status}
	err := store.Init()
	return store, err
}

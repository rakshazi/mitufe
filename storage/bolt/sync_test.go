package bolt

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"gitlab.com/rakshazi/mitufe/api"
	"gitlab.com/rakshazi/mitufe/config"
	"go.etcd.io/bbolt"
	miniflux "miniflux.app/client"
)

func TestSync(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/" + file)
	})
	var respFeeds string = `[{"id":1,"user_id":1,"feed_url":"https://not.exists/feed.rss","site_url":"https://not.exists","title":"Test","checked_at":"2021-04-02T13:35:08.449623+03:00","next_check_at":"0001-01-01T00:00:00Z","etag_header":"W/\"605d71a5-75db5\"","last_modified_header":"Fri, 26 Mar 2021 05:31:17 UTC","parsing_error_message":"","parsing_error_count":0,"scraper_rules":"","rewrite_rules":"","crawler":false,"blocklist_rules":"","keeplist_rules":"","user_agent":"","username":"","password":"","disabled":false,"ignore_http_cache":false,"allow_self_signed_certificates":false,"fetch_via_proxy":false,"category":{"id":1,"title":"Test","user_id":1},"icon":{"feed_id":1,"icon_id":0}}]`
	var respEntries string = `{"total":1,"entries":[{"id":1,"user_id":1,"feed_id":1,"status":"unread","hash":"3bbfc43bee261b284b7696e3c9755fb297ddb9f4db923ca7a5b295dfc3ead079","title":"Test","url":"http://not.exists/","comments_url":"https://not.exists#comments","published_at":"2021-03-31T20:24:54+03:00","created_at":"2021-04-02T14:35:09.343878+03:00","content":"<h1>Test</h1>","author":"test","share_code":"","starred":false,"reading_time":1,"enclosures":null,"feed":{"id":1,"user_id":1,"feed_url":"https://not.exists/rss","site_url":"https://not.exists","title":"Test","checked_at":"2021-04-02T14:35:08.431201+03:00","next_check_at":"0001-01-01T00:00:00Z","etag_header":"","last_modified_header":"","parsing_error_message":"","parsing_error_count":0,"scraper_rules":"","rewrite_rules":"","crawler":false,"blocklist_rules":"","keeplist_rules":"","user_agent":"","username":"","password":"","disabled":false,"ignore_http_cache":false,"allow_self_signed_certificates":false,"fetch_via_proxy":false,"category":{"id":1,"title":"Test","user_id":1},"icon":{"feed_id":1,"icon_id":1}}}]}`
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/v1/feeds":
			fmt.Fprintln(w, respFeeds)
		case "/v1/entries":
			fmt.Fprintln(w, respEntries)
		default:
			fmt.Fprintln(w, "I'm a teapot")
		}
	}))
	defer server.Close()
	tests := []struct {
		Name   string
		Server string
	}{
		{"ServerDown", "https://not.exists"},
		{"ServerUp", server.URL},
	}
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			config := &config.Config{Server: test.Server, Token: "test", Sort: config.Sort{Order: "published_at", Direction: "asc"}, DateFormat: "02 Jan 06 15:04"}
			client := api.New(config)
			status := make(chan string)
			store := &Bolt{App: "test", Client: client, Status: status}
			go func() {
				for message := range status {
					t.Log(message)
				}
			}()
			if err := store.Init(); err != nil {
				t.Error(err)
			}
			defer store.Close()
			if err := store.Sync(config.DateFormat, &config.Sort); err != nil {
				t.Fail()
			}
		})
	}
}

func TestSaveEntries(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/" + file)
	})
	config := &config.Config{Server: "https://not.exists", Token: "test"}
	client := api.New(config)
	store := &Bolt{App: "test", Client: client, Status: make(chan string)}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()
	entriesToSave := miniflux.Entries{&miniflux.Entry{
		ID:        int64(1),
		UserID:    int64(1),
		FeedID:    int64(1),
		Status:    "unread",
		Hash:      "smthing",
		Title:     "Test",
		URL:       "https://not.exists/article",
		Date:      time.Now(),
		CreatedAt: time.Now(),
		Content:   "<html><head><title>Test</title></head><body><h1>Test</h1></body></html>",
		Author:    "Test",
		Feed: &miniflux.Feed{
			Category: &miniflux.Category{},
		},
	}}
	if err := store.SaveEntries(entriesToSave, "02 Jan 06 15:04"); err != nil {
		t.Error(err)
	}
	entries, err := store.GetEntries("unread")
	if err != nil {
		t.Error(err)
	}
	if entries.Len() != 1 {
		t.Error("Wrong amount of unread entries")
	}
}

func TestSendReadEntries(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/" + file)
	})
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "")
	}))
	defer server.Close()
	config := &config.Config{Server: server.URL, Token: "test"}
	client := api.New(config)
	store := &Bolt{App: "test", Client: client, Status: make(chan string)}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()

	err := store.DB.Update(func(tx *bbolt.Tx) error {
		entry := &miniflux.Entry{ID: int64(1)}
		data, err := json.Marshal(entry)
		if err != nil {
			return err
		}
		return tx.Bucket([]byte(BucketEntriesRead)).Put(ConvertID(int64(1)), data)
	})
	if err != nil {
		t.Error(err)
	}
	if err := store.SendReadEntries(); err != nil {
		t.Error(err)
	}
	entries, err := store.GetEntries("read")
	if err != nil {
		t.Error(err)
	}
	if entries.Len() != 0 {
		t.Error("Read entry was not removed")
	}
}

func TestSyncFeeds(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/" + file)
	})
	config := &config.Config{Server: "http://not.exist", Token: "test"}
	client := api.New(config)
	store := &Bolt{App: "test", Client: client, Status: make(chan string)}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()
	feedsToSave := miniflux.Feeds{&miniflux.Feed{ID: int64(1), Category: &miniflux.Category{Title: "Test"}, Title: "test"}}
	err := store.SyncFeeds(feedsToSave)
	if err != nil {
		t.Error(err)
	}
	feeds, err := store.GetFeeds()
	if err != nil {
		t.Error(err)
	}
	if feeds.Len() > 1 {
		t.Error("Too many feeds in local db")
	}
	if _, ok := feeds.Get("Test / test"); !ok {
		t.Fail()
	}
}

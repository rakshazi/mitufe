package bolt

import (
	"encoding/json"
	"os"
	"testing"

	"gitlab.com/rakshazi/mitufe/api"
	"gitlab.com/rakshazi/mitufe/config"
	"go.etcd.io/bbolt"
	miniflux "miniflux.app/client"
)

func TestGetEntries(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/" + file)
	})
	config := &config.Config{Server: "http://not.exist", Token: "test"}
	client := api.New(config)
	store := &Bolt{App: "test", Client: client, Status: make(chan string)}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()
	entry := &Entry{FeedID: "1", Title: "Test", Content: "Test content"}
	err := store.DB.Update(func(tx *bbolt.Tx) error {
		data, err := json.Marshal(entry)
		if err != nil {
			t.Error(err)
		}
		return tx.Bucket([]byte(BucketEntriesUnread)).Put([]byte("1"), data)
	})
	if err != nil {
		t.Error(err)
	}
	entries, err := store.GetEntries("unread")
	if err != nil {
		t.Error(err)
	}
	item, ok := entries.Get("1")
	if item == nil || !ok {
		t.Fail()
	}
	if item.(*Entry).Title != "Test" {
		t.Fail()
	}
	store.DB.Update(func(tx *bbolt.Tx) error {
		return tx.Bucket([]byte(BucketEntriesUnread)).Delete([]byte("1"))
	})
}

func TestMarkEntry(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/" + file)
	})
	config := &config.Config{Server: "http://not.exist", Token: "test"}
	client := api.New(config)
	store := &Bolt{App: "test", Client: client, Status: make(chan string)}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()
	err := store.MarkEntry("1", "read")
	if err == nil {
		t.Error("Expected to fail")
	}
	entry := &miniflux.Entry{ID: int64(1)}
	err = store.DB.Update(func(tx *bbolt.Tx) error {
		data, err := json.Marshal(entry)
		if err != nil {
			t.Error(err)
		}
		return tx.Bucket([]byte(BucketEntriesUnread)).Put(ConvertID(entry.ID), data)
	})
	if err != nil {
		t.Error(err)
	}
	err = store.MarkEntry("1", "read")
	if err != nil {
		t.Error(err)
	}
	err = store.MarkEntry("1", "unread")
	if err != nil {
		t.Error(err)
	}
}

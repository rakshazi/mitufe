package bolt

import (
	"bytes"
	"math"
	"os"
	"strconv"
	"testing"
	"time"

	"gitlab.com/rakshazi/mitufe/config"
	"gitlab.com/rakshazi/mitufe/logger"
)

func TestMain(m *testing.M) {
	cfgPath, _ := config.GetPath("test")
	logPath, _ := logger.GetPath("test")
	for _, path := range []string{cfgPath, logPath} {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			os.Mkdir(path, 0700)
		}
	}
	defer os.Remove(cfgPath + "/local.db")
	defer os.Remove(logPath + time.Now().Format("/2006-01-02.log"))
	os.Exit(m.Run())
}

func TestConvertID(t *testing.T) {
	MaxInt64String := strconv.Itoa(int(math.MaxInt64))
	tests := []struct {
		Int64 int64
		Byte  []byte
	}{
		{int64(0), []byte("0")},
		{int64(123), []byte("123")},
		{int64(math.MaxInt64), []byte(MaxInt64String)},
	}
	for _, test := range tests {
		t.Run(string(test.Byte), func(t *testing.T) {
			id := ConvertID(test.Int64)
			if !bytes.Equal(test.Byte, id) {
				t.Errorf("Original = %v, expected = %v, got = %v", test.Int64, string(test.Byte), string(id))
			}
		})
	}
}

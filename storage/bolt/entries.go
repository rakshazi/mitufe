package bolt

import (
	"encoding/json"
	"fmt"

	"github.com/elliotchance/orderedmap"
	"go.etcd.io/bbolt"
)

// BucketEntriesUnread - unread entries bucket
const BucketEntriesUnread string = "entries/unread"

// BucketEntriesRead - read entries bucket
const BucketEntriesRead string = "entries/read"

// GetEntries from store
func (s *Bolt) GetEntries(status string) (*orderedmap.OrderedMap, error) {
	entries := orderedmap.NewOrderedMap()
	return entries, s.DB.View(func(tx *bbolt.Tx) error {
		return tx.Bucket([]byte("entries/" + status)).ForEach(func(id, item []byte) error {
			entry := &Entry{}
			err := json.Unmarshal(item, &entry)
			if err != nil {
				return err
			}
			entries.Set(string(id), entry)
			return nil
		})
	})
}

// MarkEntry - mark entry as read or unread in local db
func (s *Bolt) MarkEntry(id string, status string) error {
	var from string
	var to string
	switch status {
	case "read":
		from = BucketEntriesUnread
		to = BucketEntriesRead
	case "unread":
		from = BucketEntriesRead
		to = BucketEntriesUnread
	}

	return s.DB.Batch(func(tx *bbolt.Tx) error {
		data := tx.Bucket([]byte(from)).Get([]byte(id))
		if data == nil {
			return fmt.Errorf("entry #" + id + " not found in " + from)
		}
		err := tx.Bucket([]byte(to)).Put([]byte(id), data)
		if err != nil {
			return err
		}
		return tx.Bucket([]byte(from)).Delete([]byte(id))
	})
}

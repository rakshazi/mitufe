package bolt

import (
	"encoding/json"
	"os"
	"testing"

	"gitlab.com/rakshazi/mitufe/api"
	"gitlab.com/rakshazi/mitufe/config"
	"go.etcd.io/bbolt"
	miniflux "miniflux.app/client"
)

func TestGetFeeds(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/" + file)
	})
	config := &config.Config{Server: "http://not.exist", Token: "test"}
	client := api.New(config)
	store := &Bolt{App: "test", Client: client, Status: make(chan string)}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()
	feed := &miniflux.Feed{ID: int64(1), Title: "Test"}
	err := store.DB.Update(func(tx *bbolt.Tx) error {
		return tx.Bucket([]byte(BucketFeeds)).Put(ConvertID(feed.ID), []byte(feed.Title))
	})
	if err != nil {
		t.Error(err)
	}
	feeds, err := store.GetFeeds()
	if err != nil {
		t.Error(err)
	}
	item, ok := feeds.Get("Test")
	if !ok || item.(string) != "1" {
		t.Error("Feed not found")
	}
}

func TestDeleteFeeds(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/" + file)
	})
	config := &config.Config{Server: "http://not.exist", Token: "test"}
	client := api.New(config)
	store := &Bolt{App: "test", Client: client, Status: make(chan string)}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()
	feed := &miniflux.Feed{ID: int64(1)}
	err := store.DB.Update(func(tx *bbolt.Tx) error {
		data, err := json.Marshal(feed)
		if err != nil {
			t.Error(err)
		}
		return tx.Bucket([]byte(BucketFeeds)).Put(ConvertID(feed.ID), data)
	})
	if err != nil {
		t.Error(err)
	}
	err = store.DeleteFeeds()
	if err != nil {
		t.Error(err)
	}
}

package bolt

import (
	"sort"

	"github.com/elliotchance/orderedmap"
	"go.etcd.io/bbolt"
)

// BucketFeeds - feeds bucket
const BucketFeeds string = "feeds"

// GetFeeds from store
func (s *Bolt) GetFeeds() (*orderedmap.OrderedMap, error) {
	feeds := orderedmap.NewOrderedMap()
	unsorted := orderedmap.NewOrderedMap()
	err := s.DB.View(func(tx *bbolt.Tx) error {
		return tx.Bucket([]byte(BucketFeeds)).ForEach(func(id, title []byte) error {
			unsorted.Set(string(id), string(title))
			return nil
		})
	})
	if err != nil {
		return unsorted, err
	}
	// Sort feed alphabetically
	keys := []string{}
	vals := map[string]string{}
	for item := unsorted.Front(); item != nil; item = item.Next() {
		key := item.Key.(string)
		val := item.Value.(string)
		vals[val] = key
		keys = append(keys, val)
	}
	sort.Strings(keys)
	for _, val := range keys {
		feeds.Set(val, vals[val])
	}

	return feeds, err
}

// DeleteFeeds removes all feeds
func (s *Bolt) DeleteFeeds() error {
	return s.DB.Update(func(tx *bbolt.Tx) error {
		if err := tx.DeleteBucket([]byte(BucketFeeds)); err != nil {
			return err
		}
		_, err := tx.CreateBucket([]byte(BucketFeeds))
		return err
	})
}

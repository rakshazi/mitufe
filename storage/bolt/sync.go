package bolt

import (
	"encoding/json"
	"strconv"
	"strings"
	"sync"

	"github.com/jaytaylor/html2text"
	"gitlab.com/rakshazi/mitufe/config"
	"go.etcd.io/bbolt"
	miniflux "miniflux.app/client"
)

// Sync local store
func (s *Bolt) Sync(dateFormat string, sort *config.Sort) error {
	s.Status <- "[storage] sync started"
	var wg sync.WaitGroup
	wg.Add(3)
	go s.runFeedsSync(&wg)
	go s.runReadEntriesSync(&wg)
	go s.runUnreadEntriesSync(dateFormat, sort, &wg)

	wg.Wait()
	s.Status <- "[storage] sync completed"
	return nil
}

func (s *Bolt) runFeedsSync(wg *sync.WaitGroup) {
	s.Status <- "[storage] feeds sync started"
	feeds, err := s.Client.Feeds()
	if err != nil {
		s.Status <- "[storage] feeds sync failed: " + err.Error()
		wg.Done()
		return
	}
	if err = s.SyncFeeds(feeds); err != nil {
		s.Status <- "[storage] feeds sync failed: " + err.Error()
		wg.Done()
		return
	}
	s.Status <- "[storage] feeds sync completed"
	wg.Done()
}
func (s *Bolt) runReadEntriesSync(wg *sync.WaitGroup) {
	s.Status <- "[storage] read entries sync started"
	err := s.SendReadEntries()
	if err != nil {
		s.Status <- "[storage] read entries sync failed: " + err.Error()
	} else {
		s.Status <- "[storage] read entries sync completed"
	}
	wg.Done()
}

func (s *Bolt) runUnreadEntriesSync(dateFormat string, sort *config.Sort, wg *sync.WaitGroup) {
	s.Status <- "[storage] unread entries sync started"
	filter := &miniflux.Filter{
		Status:    "unread",
		Order:     sort.Order,
		Direction: sort.Direction,
	}
	entries, err := s.Client.Entries(filter)
	if err != nil {
		s.Status <- "[storage] unread entries sync failed: " + err.Error()
		wg.Done()
		return
	}

	s.Status <- "[storage] unread entries sync saving to local storage"
	err = s.SaveEntries(entries.Entries, dateFormat)
	if err != nil {
		s.Status <- "[storage] unread entries sync failed: " + err.Error()
	} else {
		s.Status <- "[storage] unread entries sync done"
	}
	wg.Done()
}

// SendReadEntries to miniflux (from entries/read)
func (s *Bolt) SendReadEntries() error {
	ids := []int64{}
	// Get ids of all read entries
	err := s.DB.View(func(tx *bbolt.Tx) error {
		err := tx.Bucket([]byte(BucketEntriesRead)).ForEach(func(key, _ []byte) error {
			id, err := strconv.Atoi(string(key))
			if err != nil {
				return err
			}

			ids = append(ids, int64(id))
			return nil
		})

		return err
	})
	if err != nil {
		return err
	}
	// Mark read entries on Miniflux
	if len(ids) > 0 {
		err := s.Client.UpdateEntries(ids, "read")
		if err != nil {
			return err
		}
	}

	// Remove read entries from local db
	return s.DB.Update(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketEntriesRead))
		return bucket.ForEach(func(key, _ []byte) error {
			return bucket.Delete(key)
		})
	})
}

// SaveEntries to entries/unread
func (s *Bolt) SaveEntries(entries miniflux.Entries, dateFormat string) error {
	var err error
	var exists bool
	for _, minifluxEntry := range entries {
		exists = true
		entryID := ConvertID(minifluxEntry.ID)
		s.DB.View(func(tx *bbolt.Tx) error {
			if item := tx.Bucket([]byte(BucketEntriesUnread)).Get(entryID); item == nil {
				exists = false
			}
			return nil
		})
		if exists {
			continue
		}
		var content strings.Builder
		text, err := html2text.FromString(minifluxEntry.Content, html2text.Options{PrettyTables: true})
		content.WriteString(minifluxEntry.Feed.Category.Title + " >> " + minifluxEntry.Feed.Title + " >> " + minifluxEntry.Title)
		content.WriteString(" by " + minifluxEntry.Author)
		content.WriteString(" " + minifluxEntry.Date.Format(dateFormat))
		content.WriteString("\n\n" + text)
		if err != nil {
			return err
		}
		entry := Entry{
			URL:     minifluxEntry.URL,
			Title:   minifluxEntry.Title,
			FeedID:  strconv.Itoa(int(minifluxEntry.FeedID)),
			Content: content.String(),
		}
		err = s.DB.Update(func(tx *bbolt.Tx) error {
			data, err := json.Marshal(entry)
			if err != nil {
				return err
			}
			return tx.Bucket([]byte(BucketEntriesUnread)).Put(entryID, data)
		})
		if err != nil {
			return err
		}
	}
	return err
}

// SyncFeeds - save new feeds and remove old from db
func (s *Bolt) SyncFeeds(feeds miniflux.Feeds) error {
	if err := s.DeleteFeeds(); err != nil {
		return err
	}

	return s.DB.Batch(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketFeeds))
		// Add new feeds
		for _, feed := range feeds {
			id := ConvertID(feed.ID)
			if err := bucket.Put(id, []byte(feed.Category.Title+" / "+feed.Title)); err != nil {
				return err
			}
		}
		return nil
	})
}

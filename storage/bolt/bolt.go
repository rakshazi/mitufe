package bolt

import (
	"os"
	"strconv"

	"gitlab.com/rakshazi/mitufe/api"
	"gitlab.com/rakshazi/mitufe/config"
	"go.etcd.io/bbolt"
)

// Bolt - bbolt db store
type Bolt struct {
	DB     *bbolt.DB
	App    string
	Client api.Client
	Status chan string
}

// Entry - stored article
type Entry struct {
	FeedID  string `json:"feed_id"`
	URL     string `json:"url"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

// File name
var file string = "local.db"

// ConvertID - converts int64 from Miniflux to []byte for bbolt db
func ConvertID(id int64) []byte {
	return []byte(strconv.Itoa(int(id)))
}

// Init store
func (s *Bolt) Init() error {
	err := s.openDB()
	if err != nil {
		return err
	}
	err = s.initDB()
	return err
}

// openDB - opens bolt db
func (s *Bolt) openDB() error {
	path, err := config.GetPath(s.App)
	if err != nil {
		return err
	}

	db, err := bbolt.Open(path+"/"+file, 0600, nil)
	if err != nil {
		return err
	}
	s.DB = db

	return nil
}

// initDB - init db structure
func (s *Bolt) initDB() error {
	buckets := []string{"feeds", "entries/unread", "entries/read"}

	tx, err := s.DB.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()
	for _, bucket := range buckets {
		_, err := tx.CreateBucketIfNotExists([]byte(bucket))
		if err != nil {
			return err
		}
	}

	return tx.Commit()
}

// compactDB compacts database
func (s *Bolt) compactDB() error {
	path, err := config.GetPath(s.App)
	if err != nil {
		return err
	}

	srcPath := path + "/" + file
	dstPath := srcPath + ".tmp"

	src, err := bbolt.Open(srcPath, 0600, nil)
	if err != nil {
		src.Close()
		return err
	}

	dst, err := bbolt.Open(dstPath, 0600, nil)
	if err != nil {
		dst.Close()
		return err
	}

	err = bbolt.Compact(dst, src, 65536)
	src.Close()
	dst.Close()
	if err != nil {
		return err
	}
	if err := os.Remove(srcPath); err != nil {
		return err
	}

	return os.Rename(dstPath, srcPath)
}

// Close the db
func (s *Bolt) Close() error {
	close(s.Status)
	if err := s.DB.Close(); err != nil {
		return err
	}
	return s.compactDB()
}

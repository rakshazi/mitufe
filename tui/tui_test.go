package tui

import (
	"encoding/json"
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/rakshazi/mitufe/api"
	"gitlab.com/rakshazi/mitufe/config"
	"gitlab.com/rakshazi/mitufe/logger"
	"gitlab.com/rakshazi/mitufe/storage/bolt"
	"go.etcd.io/bbolt"
)

func TestMain(m *testing.M) {
	cfgPath, _ := config.GetPath("test")
	logPath, _ := logger.GetPath("test")
	for _, path := range []string{cfgPath, logPath} {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			os.Mkdir(path, 0700)
		}
	}
	defer os.Remove(cfgPath + "/local.db")
	defer os.Remove(logPath + time.Now().Format("/2006-01-02.log"))
	os.Exit(m.Run())
}

func TestLoadData(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/local.db")
	})
	config := &config.Config{Server: "https://not.exist", Token: "test", Sort: config.Sort{Order: "published_at", Direction: "asc"}, DateFormat: "02 Jan 06 15:04"}
	client := api.New(config)
	statusChan := make(chan string)
	store = &bolt.Bolt{App: "test", Client: client, Status: statusChan}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()
	err := store.(*bolt.Bolt).DB.Update(func(tx *bbolt.Tx) error {
		return tx.Bucket([]byte(bolt.BucketFeeds)).Put([]byte("1"), []byte("Category / Title"))
		return nil
	})

	if err != nil {
		t.Error(err)
	}

	if err := LoadData(); err != nil {
		t.Error(err)
	}
	if len(entries) == 0 {
		t.Error("entries map is empty")
	}
	if sidebar.GetItemCount() != 2 {
		t.Error("sidebar should have 2 feeds - 'All feeds' and 'Title'")
	}
	tests := []struct {
		Name  string
		Index int
		Text  string
		ID    string
	}{
		{"first", 0, "All feeds", "0"},
		{"second", 1, "Category / Title", "1"},
	}

	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			text, id := sidebar.GetItemText(test.Index)
			if text != test.Text {
				t.Error(test.Name + " element of sidebar should have text = " + test.Text)
			}
			if id != test.ID {
				t.Error(test.Name + " element of sidebar should have secondary text (id) = " + test.ID)
			}
		})
	}
}

func TestToggleEntries(t *testing.T) {
	t.Cleanup(func() {
		path, _ := config.GetPath("test")
		os.Remove(path + "/local.db")
	})
	config := &config.Config{Server: "https://not.exist", Token: "test", Sort: config.Sort{Order: "published_at", Direction: "asc"}, DateFormat: "02 Jan 06 15:04"}
	client := api.New(config)
	statusChan := make(chan string)
	store = &bolt.Bolt{App: "test", Client: client, Status: statusChan}
	if err := store.Init(); err != nil {
		t.Error(err)
	}
	defer store.Close()
	err := store.(*bolt.Bolt).DB.Update(func(tx *bbolt.Tx) error {
		entry := bolt.Entry{FeedID: "1", Title: "Test Unread"}
		data, err := json.Marshal(entry)
		if err != nil {
			t.Error(err)
		}
		err = tx.Bucket([]byte(bolt.BucketEntriesUnread)).Put([]byte("1"), data)
		if err != nil {
			return err
		}
		entry.Title = "Test Read"
		data, err = json.Marshal(entry)
		if err != nil {
			t.Error(err)
		}
		return tx.Bucket([]byte(bolt.BucketEntriesRead)).Put([]byte("1"), data)
	})

	if err != nil {
		t.Error(err)
	}

	if err := LoadData(); err != nil {
		t.Error(err)
	}

	status = ""
	tests := []string{"", "read", "unread"}
	for _, test := range tests {
		t.Run(test, func(t *testing.T) {
			if err := ToggleEntries(true); err != nil {
				t.Error(err)
			}
			if main.GetTitle() != strings.ToUpper(status)+" entries" {
				t.Error("main title should be changed")
			}
			expected := strings.Title("test " + status)
			title, id := main.GetItemText(0)
			if title != expected {
				t.Error("title should be = " + expected)
			}
			if id != "1" {
				t.Error("secondary text (id) should be = 1")
			}
		})
	}
}

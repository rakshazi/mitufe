package tui

import (
	"github.com/gdamore/tcell/v2"
	"github.com/pkg/browser"
	"gitlab.com/rakshazi/mitufe/storage/bolt"
)

func mainInputCapture(event *tcell.EventKey) *tcell.EventKey {
	switch event.Rune() {
	case hotkeys["close"]:
		app.Stop()
	case 0:
		// ESC
		if event.Key() == 27 {
			app.Stop()
		}
	case hotkeys["down"]:
		main.SetCurrentItem(main.GetCurrentItem() + 1)
	case 10: // Ctrl+j
		sidebar.SetCurrentItem(sidebar.GetCurrentItem() + 1)
	case hotkeys["up"]:
		main.SetCurrentItem(main.GetCurrentItem() - 1)
	case 11: // Ctrl+k
		sidebar.SetCurrentItem(sidebar.GetCurrentItem() - 1)
	case hotkeys["top"]:
		main.SetCurrentItem(0)
	case 7: // Ctrl+g
		sidebar.SetCurrentItem(0)
	case hotkeys["bottom"]:
		main.SetCurrentItem(main.GetItemCount() - 1)
	case hotkeys["toggle"]:
		ToggleEntries(true)
	}
	return event
}

func mainSelected(index int, title string, id string, shortcut rune) {
	article.SetTitle(title)
	entry, _ := entries[status].Get(id)
	article.SetText(entry.(*bolt.Entry).Content)
	article.ScrollToBeginning()
	hotkeybar.SetText(articleHotkeys)
	pages.SwitchToPage("article")
	app.SetFocus(article)
	entryURL = entry.(*bolt.Entry).URL

	if status == "unread" {
		go store.MarkEntry(id, "read")
		entries["read"].Set(id, entry.(*bolt.Entry))
		entries["unread"].Delete(id)
		main.RemoveItem(index)
		main.SetCurrentItem(index)
	}
}

func articleInputCapture(event *tcell.EventKey) *tcell.EventKey {
	switch event.Rune() {
	case hotkeys["close"]:
		hotkeybar.SetText(mainHotkeys)
		pages.SwitchToPage("main")
		app.SetFocus(main)
	case hotkeys["open_in_browser"]:
		browser.OpenURL(entryURL)
	case hotkeys["next_article"]:
		index := main.GetCurrentItem()
		title, id := main.GetItemText(index)
		mainSelected(index, title, id, 0)
	case 0:
		// Esc
		if event.Key() == 27 {
			pages.SwitchToPage("main")
			app.SetFocus(main)
		}
	}
	return event
}

func sidebarChanged(index int, mainText string, secondaryText string, shortcut rune) {
	// On early init, we don't have entries yet, so just ignore that function call
	if len(entries) == 0 {
		return
	}
	feedID = secondaryText
	ToggleEntries(false)
}

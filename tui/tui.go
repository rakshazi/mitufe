package tui

import (
	"strings"

	"github.com/elliotchance/orderedmap"
	"github.com/rivo/tview"
	"gitlab.com/rakshazi/mitufe/storage"
	"gitlab.com/rakshazi/mitufe/storage/bolt"
)

// Storage controller
var store storage.Store

// app - tview application
var app *tview.Application

// layouts
var layoutMain, layoutMainContent, layoutArticle *tview.Flex

// Pages
var pages *tview.Pages

// Sidebar
var sidebar *tview.List

// Main content
var main *tview.List

// Status bar
var statusbar *tview.TextView

// Hotkeys bar, under the statusbar
var hotkeybar *tview.TextView

// Article view
var article *tview.TextView

// Hotkeys map
var hotkeys map[string]rune = make(map[string]rune)

// Entries status/type: read or unread
var status string = "unread"

// Current feed id
var feedID string = "0"

// Current entry url
var entryURL string

// Entries map
var entries = make(map[string]*orderedmap.OrderedMap)

// text for hotkeybar on main screen
var mainHotkeys, articleHotkeys string

// init TUI layout
func init() {
	app = tview.NewApplication()

	sidebar = tview.NewList().ShowSecondaryText(false)
	sidebar.SetBorder(true).SetTitle("Feeds")
	sidebar.SetChangedFunc(sidebarChanged)

	main = tview.NewList().ShowSecondaryText(false)
	main.SetBorder(true).SetTitle("Entries")
	main.SetHighlightFullLine(true)
	main.SetInputCapture(mainInputCapture)
	main.SetSelectedFunc(mainSelected)

	article = tview.NewTextView().SetWordWrap(true)
	article.SetBorder(true)
	article.SetChangedFunc(func() { app.Draw() })
	article.SetInputCapture(articleInputCapture)

	statusbar = tview.NewTextView().SetWordWrap(true)
	hotkeybar = tview.NewTextView()
	hotkeybar.SetTextAlign(tview.AlignCenter)

	layoutMain = tview.NewFlex().SetDirection(tview.FlexRow)
	layoutMainContent = tview.NewFlex()
	layoutMainContent.AddItem(sidebar, 0, 1, false)
	layoutMainContent.AddItem(main, 0, 4, true)
	layoutMain.AddItem(layoutMainContent, 0, 20, true)
	layoutMain.AddItem(statusbar, 1, 0, false)
	layoutMain.AddItem(hotkeybar, 1, 0, false)

	layoutArticle = tview.NewFlex().SetDirection(tview.FlexRow)
	layoutArticle.AddItem(article, 0, 20, true)
	layoutArticle.AddItem(hotkeybar, 1, 0, false)

	pages = tview.NewPages()
	pages.AddPage("main", layoutMain, true, true)
	pages.AddPage("article", layoutArticle, true, false)

	app.SetRoot(pages, true)
	app.EnableMouse(true)
}

// LoadData - load data from db
func LoadData() error {
	feeds, err := store.GetFeeds()
	if err != nil {
		return err
	}
	sidebar.Clear()
	sidebar.AddItem("All feeds", "0", 0, nil)
	for feed := feeds.Front(); feed != nil; feed = feed.Next() {
		title := feed.Key.(string)
		id := feed.Value.(string)
		sidebar.AddItem(title, id, 0, nil)
	}
	unreadEntries, err := store.GetEntries("unread")
	if err != nil {
		return err
	}
	readEntries, err := store.GetEntries("read")
	if err != nil {
		return err
	}
	entries["unread"] = unreadEntries
	entries["read"] = readEntries
	if err := ToggleEntries(false); err != nil {
		return err
	}

	return nil
}

// ToggleEntries - show entries of different status
func ToggleEntries(toggleStatus bool) error {
	if toggleStatus {
		switch status {
		case "read":
			status = "unread"
		case "unread":
			status = "read"
		default:
			status = "unread"
		}
	}

	main.SetTitle(strings.ToUpper(status) + " entries")
	main.Clear()

	for item := entries[status].Front(); item != nil; item = item.Next() {
		if feedID == "0" || item.Value.(*bolt.Entry).FeedID == feedID {
			main.AddItem(item.Value.(*bolt.Entry).Title, item.Key.(string), 0, nil)
		}
	}

	return nil
}

// Run - fill TUI app with content and run it
func Run(storageController storage.Store) error {
	store = storageController
	if err := LoadData(); err != nil {
		return err
	}
	return app.Run()
}

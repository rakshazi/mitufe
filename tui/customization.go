package tui

import (
	"github.com/gdamore/tcell/v2"
	"gitlab.com/rakshazi/mitufe/config"
)

// SetStatus text to statusbar
func SetStatus(text string) {
	statusbar.SetText(text)
	// "Hook"/hack to re-load fresh synced data to TUI
	if text == "[storage] sync completed" {
		LoadData()
	}
	app.Draw()
}

// Set TUI colors
func SetColors(colors config.Colors) {
	sidebar.SetBackgroundColor(tcell.GetColor(colors.SidebarBackground))
	sidebar.SetMainTextColor(tcell.GetColor(colors.SidebarText))

	main.SetBackgroundColor(tcell.GetColor(colors.MainBackground))
	main.SetMainTextColor(tcell.GetColor(colors.MainText))

	article.SetBackgroundColor(tcell.GetColor(colors.ArticleBackground))
	article.SetTextColor(tcell.GetColor(colors.ArticleText))

	statusbar.SetBackgroundColor(tcell.GetColor(colors.StatusbarBackground))
	statusbar.SetTextColor(tcell.GetColor(colors.StatusbarText))

	hotkeybar.SetBackgroundColor(tcell.GetColor(colors.HotkeybarBackground))
	hotkeybar.SetTextColor(tcell.GetColor(colors.HotkeybarText))
}

// SetElements that should be enabled/disabled
func SetElements(elements config.Elements) {
	if !elements.Sidebar {
		layoutMainContent.RemoveItem(sidebar)
	}
	if !elements.Statusbar {
		layoutMain.RemoveItem(statusbar)
	}
	if !elements.Hotkeybar {
		layoutMain.RemoveItem(hotkeybar)
		layoutArticle.RemoveItem(hotkeybar)
	}
}

// SetHotkeys from config
func SetHotkeys(keys config.Hotkeys) {
	hotkeys["up"] = []rune(keys.Up)[0]
	hotkeys["down"] = []rune(keys.Down)[0]
	hotkeys["top"] = []rune(keys.Top)[0]
	hotkeys["bottom"] = []rune(keys.Bottom)[0]
	hotkeys["toggle"] = []rune(keys.Toggle)[0]
	hotkeys["close"] = []rune(keys.Close)[0]
	hotkeys["next_article"] = []rune(keys.NextArticle)[0]
	hotkeys["open_in_browser"] = []rune(keys.OpenInBrowser)[0]

	mainHotkeys = keys.Down + " down | " + keys.Up + " up | " + keys.Top + " top | " + keys.Bottom + " bottom | "
	mainHotkeys += keys.Toggle + " toggle entries | Ctrl+j next feed | Ctrl+k prev feed | Ctrl+g first feed | " + keys.Close + " exit"

	articleHotkeys = keys.Down + " down | " + keys.Up + " up | " + keys.Top + " top | " + keys.Bottom + " bottom | "
	articleHotkeys += keys.OpenInBrowser + " open in browser | " + keys.NextArticle + " next | " + keys.Close + " close article"

	hotkeybar.SetText(mainHotkeys)
}

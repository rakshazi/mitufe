package api

import (
	"gitlab.com/rakshazi/mitufe/config"
	miniflux "miniflux.app/client"
)

// Client - API client interface
type Client interface {
	Me() (*miniflux.User, error)
	Feeds() (miniflux.Feeds, error)
	Entries(*miniflux.Filter) (*miniflux.EntryResultSet, error)
	UpdateEntries([]int64, string) error
}

// New miniflux client
func New(cfg *config.Config) Client {
	if len(cfg.Token) > 0 {
		return miniflux.New(cfg.Server, cfg.Token)
	}
	return miniflux.New(cfg.Server, cfg.Login, cfg.Password)
}

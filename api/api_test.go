package api

import (
	"testing"

	"gitlab.com/rakshazi/mitufe/config"
	"miniflux.app/client"
)

func TestNew(t *testing.T) {
	tests := []struct {
		Name   string
		Config *config.Config
	}{
		{"token", &config.Config{Server: "http://not.exist", Token: "test"}},
		{"login-password", &config.Config{Server: "http://not.exist", Login: "test", Password: "test"}},
	}
	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			api := New(test.Config)
			if _, ok := api.(*client.Client); !ok {
				t.Fail()
			}
		})
	}
}

FROM alpine:latest AS builder

RUN apk --no-cache add git ca-certificates tzdata && update-ca-certificates && \
    adduser -D -g '' mitufe && \
    mkdir -p /home/mitufe/.config/mitufe && \
    mkdir -p /home/mitufe/.cache/mitufe

FROM scratch

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY mitufe /bin/mitufe
COPY --from=builder /home /home

USER mitufe
ENTRYPOINT ["/bin/mitufe"]
